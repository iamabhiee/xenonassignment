//
//  main.m
//  XenonAssignment
//
//  Created by Abhishek Sheth on 12/07/15.
//  Copyright (c) 2015 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
