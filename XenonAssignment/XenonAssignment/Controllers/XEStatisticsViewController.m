//
//  XEStatisticsViewController.m
//  XenonAssignment
//
//  Created by Abhishek Sheth on 12/07/15.
//  Copyright (c) 2015 Abhishek. All rights reserved.
//

#import "XEStatisticsViewController.h"
#import "XEStatisticsViewCell.h"
#import "XEStatistics.h"
#import "XEDatabaseHelper.h"

@interface XEStatisticsViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *statistics;

@end

@implementation XEStatisticsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Get statistics from table
    self.statistics = [[XEDatabaseHelper sharedInstance] getStatistics];
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.statistics.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //Only one row per section
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    XEStatistics *statistic = self.statistics[indexPath.section];
    XEStatisticsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    [cell configureWithStatistics:statistic];
    return cell;
}

#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 210;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    XEStatistics *statistic = self.statistics[section];
    
    NSString *dateString = [NSString stringWithFormat:@"%@ - %@", statistic.startDate, statistic.endDate];
    return dateString;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    XEStatistics *statistic = self.statistics[indexPath.section];
    
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:@[[statistic shareMessage]]
                                      applicationActivities:nil];
    [self.navigationController presentViewController:activityViewController
                                       animated:YES
                                     completion:^{
                                         // ...
                                     }];
}

@end
