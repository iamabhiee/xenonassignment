//
//  XETrackingViewController.m
//  XenonAssignment
//
//  Created by Abhishek Sheth on 12/07/15.
//  Copyright (c) 2015 Abhishek. All rights reserved.
//

#import "XETrackingViewController.h"

#import "XELocation.h"
#import "XESession.h"
#import "XEAcceleration.h"

#import "XEDatabaseHelper.h"
#import "CLLocation+XLAdditions.h"

@import GoogleMaps;
@import CoreMotion;


@interface XETrackingViewController () <CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblSpeed;
@property (weak, nonatomic) IBOutlet UILabel *lblX;
@property (weak, nonatomic) IBOutlet UILabel *lblY;
@property (weak, nonatomic) IBOutlet UILabel *lblZ;
@property (weak, nonatomic) IBOutlet UIButton *btnTracking;


@property (nonatomic, strong) CLLocationManager *locationManager;
@property (strong, nonatomic) CMMotionManager *motionManager;

@property (nonatomic, strong) NSMutableArray *locations;
@property (nonatomic, strong) NSMutableArray *locationsToStore;
@property (nonatomic, strong) NSMutableArray *accelerationToStore;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) XESession *currentSession;

- (IBAction)actionTracking:(id)sender;

@end

@implementation XETrackingViewController

#pragma mark - View Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    //Do any additional setup after loading the view.
    
    //Init
    self.locations = [NSMutableArray new];
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self resetUI];
    
    //For iOS 8 otherwise wont work in iOS 8
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    //Accelerometer
    //Get accelerometer updates at every second
    self.motionManager = [[CMMotionManager alloc] init];
    self.motionManager.accelerometerUpdateInterval = 1;
    
    //For Accuracy
    //More accurate reading --> Consume more battery
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    //Activity type
    self.locationManager.activityType = CLActivityTypeAutomotiveNavigation;
    
    //Movement threshold for new events.
    //Will update location after every x meters
    self.locationManager.distanceFilter = 10; // meters
    
    //Default location
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:21.0 longitude:78.0 zoom:4.0];
    [self.mapView setCamera:camera];
}

#pragma mark - IBAction

- (IBAction)actionTracking:(id)sender {
    UIButton *senderButton = sender;
    
    if (senderButton.isSelected) {
        [self stopLocationUpdates];
    } else {
        [self startLocationUpdates];
    }
    
    senderButton.selected = !senderButton.isSelected;
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    //If Authorized --> Show current location
    if (status == kCLAuthorizationStatusAuthorizedAlways) {
        self.mapView.myLocationEnabled = YES;
        self.mapView.settings.myLocationButton = YES;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    for (CLLocation *newLocation in locations) {
        //Get timestamp
        NSDate *eventDate = newLocation.timestamp;
        NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
        
        //Check if it was recent (10 sec)
        //Also check the accuracy
        if (fabs(howRecent) < 10.0 && newLocation.horizontalAccuracy < 20) {
            
            if (self.locations.count > 0) {
                CLLocation *lastLocation = self.locations.lastObject;
                
                //Update Distance
                self.currentSession.distance += [newLocation distanceFromLocation:lastLocation];
                self.lblDistance.text = [NSString stringWithFormat:@"%.2f Km", self.currentSession.distance/1000];
                
                //Update Speed
                float speed = [newLocation speedInKMPH];
                self.lblSpeed.text = [NSString stringWithFormat:@"%.2f Kmph", speed];
                NSLog(@"Speed is : %f", speed);
                
                //Update Min/Max Speeds
                self.currentSession.minSpeed = self.currentSession.minSpeed > speed ? speed : self.currentSession.minSpeed;
                self.currentSession.maxSpeed = self.currentSession.maxSpeed < speed ? speed : self.currentSession.maxSpeed;
                
                //Take last location and current location coordinates
                CLLocationCoordinate2D coords[2];
                coords[0] = lastLocation.coordinate;
                coords[1] = newLocation.coordinate;
                
                //Create Path
                GMSMutablePath *path = [GMSMutablePath path];
                [path addCoordinate:coords[0]];
                [path addCoordinate:coords[1]];
                
                //Add polyline
                GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
                polyline.strokeWidth = 4.0;
                
                //Configure Polyline
                //Speed < 20 --> Green
                //20 < Speed < 40 --> Yellow
                //Speed > 40 --> Red
                if (speed < 20) {
                    polyline.strokeColor = [UIColor greenColor];
                } else if (speed < 40) {
                    polyline.strokeColor = [UIColor yellowColor];
                } else {
                    polyline.strokeColor = [UIColor redColor];
                }
                
                polyline.map = self.mapView;
                
                //Save location to databbase
                [self saveLocation:newLocation previousLocation:lastLocation];
            }
            
            //Adjust zoom level
            CGFloat zoomLevel = self.mapView.camera.zoom;
            if (zoomLevel < 8) {
                zoomLevel = 16;
            }
            
            //Update camera position
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude zoom:zoomLevel];
            [self.mapView setCamera:camera];
            
            [self.locations addObject:newLocation];
        }
    }
}


#pragma mark - Private Methods

- (void)resetUI {
    self.lblTime.text = @"N/A";
    self.lblDistance.text = @"N/A";
    self.lblSpeed.text = @"N/A";
    
    self.lblX.text = @"N/A";
    self.lblY.text = @"N/A";
    self.lblZ.text = @"N/A";
}

- (void)startLocationUpdates {
    [self resetUI];
    
    //Start new session
    self.currentSession = [XESession new];
    
    //Reset arrays
    self.locationsToStore = [NSMutableArray new];
    self.locations = [NSMutableArray new];
    self.accelerationToStore = [NSMutableArray new];
    
    //Start monitoring locations
    [self.locationManager startUpdatingLocation];
    
    //Start monitoring accelerator
    [self.motionManager startAccelerometerUpdatesToQueue:[NSOperationQueue mainQueue]
                                             withHandler:^(CMAccelerometerData  *accelerometerData, NSError *error) {
                                                 
                                                 if(error){
                                                     NSLog(@"%@", error);
                                                     return;
                                                 }
                                                 
                                                 //Store Accelertion Data
                                                 [self handleAccelertionData:accelerometerData.acceleration];
                                             }];
    
    //Start timer to update time
    dispatch_async(dispatch_get_main_queue(), ^{
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTimer:) userInfo:nil repeats:YES];
    });
}

- (void)stopLocationUpdates {
    //Stop monitoring
    [self.locationManager stopUpdatingLocation];
    [self.motionManager stopAccelerometerUpdates];
    [self.timer invalidate];
    
    //Store locations data to local database
    [self storeLocationTrackingVisitToDatabase];
}

-(void)handleAccelertionData:(CMAcceleration)acceleration {
    //Update labels
    self.lblX.text = [NSString stringWithFormat:@"%.2f", acceleration.x];
    self.lblY.text = [NSString stringWithFormat:@"%.2f", acceleration.y];
    self.lblZ.text = [NSString stringWithFormat:@"%.2f", acceleration.z];
    
    //Update min/max Acceleration values
    self.currentSession.minX = self.currentSession.minX > acceleration.x ? acceleration.x : self.currentSession.minX;
    self.currentSession.minY = self.currentSession.minY > acceleration.y ? acceleration.y : self.currentSession.minY;
    self.currentSession.minZ = self.currentSession.minZ > acceleration.z ? acceleration.z : self.currentSession.minZ;
    
    self.currentSession.maxX = self.currentSession.maxX < acceleration.x ? acceleration.x : self.currentSession.maxX;
    self.currentSession.maxY = self.currentSession.maxY < acceleration.y ? acceleration.y : self.currentSession.maxY;
    self.currentSession.maxZ = self.currentSession.maxZ < acceleration.z ? acceleration.z : self.currentSession.maxZ;
    
    //Save Acceleration
    XEAcceleration *accelerationObj = [XEAcceleration new];
    accelerationObj.x = acceleration.x;
    accelerationObj.y = acceleration.y;
    accelerationObj.z = acceleration.z;
    accelerationObj.timestamp = [[NSDate date] timeIntervalSince1970];
    [self.accelerationToStore addObject:accelerationObj];
}

- (void)saveLocation:(CLLocation *)currentLocation previousLocation:(CLLocation *)previousLocation {
    XELocation *location = [XELocation new];
    location.fromLatitude = previousLocation.coordinate.latitude;
    location.fromLongitude = previousLocation.coordinate.latitude;
    location.toLatitude = currentLocation.coordinate.latitude;
    location.toLongitude = currentLocation.coordinate.latitude;
    location.timestamp = [currentLocation.timestamp timeIntervalSince1970];
    location.speed = [currentLocation speedInKMPH];
    location.distance = [currentLocation distanceFromLocation:previousLocation];
    
    [self.locationsToStore addObject:location];
}

- (void)storeLocationTrackingVisitToDatabase {
    
    //Only if Location or Acceleration updates are received
    //Update Current session
    NSInteger timestamp = ((XELocation *)self.locationsToStore.firstObject).timestamp;
    self.currentSession.timestamp = timestamp ? timestamp : [[NSDate date] timeIntervalSince1970];
    self.currentSession.averageSpeed = (self.currentSession.distance * 3.6) / self.currentSession.duration;
    
    //Store Current Session
    [[XEDatabaseHelper sharedInstance] saveTrackingSession:self.currentSession completion:^(int64_t sessionId) {
        
        if (sessionId) {
            //Store locations if location updates received
            if (self.locationsToStore.count) {
                [[XEDatabaseHelper sharedInstance] saveLocations:self.locationsToStore sessionId:(int)sessionId];
            }
            
            //Store Accelerations if Accelerations updates received
            if (self.accelerationToStore.count) {
                [[XEDatabaseHelper sharedInstance] saveAccelerations:self.accelerationToStore sessionId:(int)sessionId];
            }
        }
    }];
}

- (void)updateTimer:(id)timer {
    //Increment duration
    self.currentSession.duration++;
    
    //Update time on screen
    double totalSeconds = self.currentSession.duration;
    double seconds = fmod(totalSeconds, 60.0);
    double minutes = fmod(trunc(totalSeconds / 60.0), 60.0);
    double hours = trunc(totalSeconds / 3600.0);
    self.lblTime.text = [NSString stringWithFormat:@"%02.0f:%02.0f:%02.0f", hours, minutes, seconds];
}
@end
