//
//  XEStatistics.h
//  XenonAssignment
//
//  Created by Abhishek Sheth on 13/07/15.
//  Copyright (c) 2015 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XEStatistics : NSObject

@property (nonatomic) double totalDistance;
@property (nonatomic) double totalDuration;
@property (nonatomic) double minSpeed;
@property (nonatomic) double maxSpeed;
@property (nonatomic) double averageSpeed;
@property (nonatomic) NSString *startDate;
@property (nonatomic) NSString *endDate;

@property (nonatomic) double maxX;
@property (nonatomic) double maxY;
@property (nonatomic) double maxZ;

@property (nonatomic) double minX;
@property (nonatomic) double minY;
@property (nonatomic) double minZ;

- (NSString *)shareMessage;

@end
