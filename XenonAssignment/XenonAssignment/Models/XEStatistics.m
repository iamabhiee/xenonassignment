//
//  XEStatistics.m
//  XenonAssignment
//
//  Created by Abhishek Sheth on 13/07/15.
//  Copyright (c) 2015 Abhishek. All rights reserved.
//

#import "XEStatistics.h"

@implementation XEStatistics

- (NSString *)shareMessage {
    return [NSString stringWithFormat:@"For the week starting from %@ I travelled %0.2f Km", self.startDate, self.totalDistance/1000];
}

@end
