//
//  XESession.h
//  XenonAssignment
//
//  Created by Abhishek Sheth on 13/07/15.
//  Copyright (c) 2015 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XESession : NSObject

@property (nonatomic) double distance;
@property (nonatomic) double duration;
@property (nonatomic) int timestamp;
@property (nonatomic) double minSpeed;
@property (nonatomic) double maxSpeed;
@property (nonatomic) double averageSpeed;

@property (nonatomic) double maxX;
@property (nonatomic) double maxY;
@property (nonatomic) double maxZ;

@property (nonatomic) double minX;
@property (nonatomic) double minY;
@property (nonatomic) double minZ;

@end
