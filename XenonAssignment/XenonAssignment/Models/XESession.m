//
//  XESession.m
//  XenonAssignment
//
//  Created by Abhishek Sheth on 13/07/15.
//  Copyright (c) 2015 Abhishek. All rights reserved.
//

#import "XESession.h"

@interface XESession()

@end

@implementation XESession

- (id)init {
    if (self = [super init]) {
        self.minSpeed = NSIntegerMax;
        self.minX = NSIntegerMax;
        self.minY = NSIntegerMax;
        self.minZ = NSIntegerMax;
        
        self.maxSpeed = NSIntegerMin;
        self.maxX = NSIntegerMin;
        self.maxY = NSIntegerMin;
        self.maxZ = NSIntegerMin;
    }
    return self;
}

@end
