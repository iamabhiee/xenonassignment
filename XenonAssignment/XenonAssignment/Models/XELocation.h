//
//  XELocation.h
//  XenonAssignment
//
//  Created by Abhishek Sheth on 13/07/15.
//  Copyright (c) 2015 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XELocation : NSObject

@property (nonatomic) double distance;
@property (nonatomic) double duration;
@property (nonatomic) int timestamp;
@property (nonatomic) double speed;
@property (nonatomic) double fromLatitude;
@property (nonatomic) double fromLongitude;
@property (nonatomic) double toLatitude;
@property (nonatomic) double toLongitude;


@end
