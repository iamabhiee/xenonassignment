//
//  XEDatabaseHelper.m
//  XenonAssignment
//
//  Created by Abhishek Sheth on 13/07/15.
//  Copyright (c) 2015 Abhishek. All rights reserved.
//

#import "XEDatabaseHelper.h"

#import "XESession.h"
#import "XELocation.h"
#import "XEStatistics.h"
#import "XEAcceleration.h"

#import "FMDatabaseQueue.h"
#import "FMDatabase.h"

#define DatabaseName @"Tracking.sqlite"

@interface XEDatabaseHelper()

@property (nonatomic, strong) FMDatabase *db;
@property (nonatomic, strong) FMDatabaseQueue *queue;
@property (nonatomic, strong) NSOperationQueue *writeQueue;
@property (nonatomic, strong) NSString *databasePath;
@end

@implementation XEDatabaseHelper

+ (instancetype)sharedInstance {
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (id)init {
    self = [super init];
    if (self) {
        _writeQueue = [[NSOperationQueue alloc] init];
        _writeQueue.maxConcurrentOperationCount = 1;
        
        NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSString *path = [documentsPath stringByAppendingPathComponent:DatabaseName];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL fileExists    = [fileManager fileExistsAtPath:path];

        //If database doesnt exist copy database from bundle
        if (!fileExists) {
            NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DatabaseName];
            [fileManager copyItemAtPath:databasePathFromApp toPath:path error:nil];
        }
        
        self.databasePath = path;
        self.db = [FMDatabase databaseWithPath:path];
        _queue = [[FMDatabaseQueue alloc] initWithPath:path];
    }
    return self;
}

#pragma mark - Locations

- (BOOL)saveLocations:(NSMutableArray *)locations sessionId:(int)sessionId {
    __block BOOL success = NO;
    for (XELocation *location in locations) {
        FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath:self.databasePath];
        
        [queue inDatabase:^(FMDatabase *db) {
            NSString *query = [[NSString alloc] initWithFormat:@"INSERT INTO Location (%@) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", @"sessionId, timestamp, fromLatitude, toLatitude, fromLongitude, toLongitude, duration, speed, distance"];
            success = [db executeUpdate:query withArgumentsInArray:@[
                                                                          @(sessionId),
                                                                          @(location.timestamp),
                                                                          @(location.fromLatitude),
                                                                          @(location.toLatitude),
                                                                          @(location.fromLongitude),
                                                                          @(location.toLongitude),
                                                                          @(location.duration),
                                                                          @(location.speed),
                                                                          @(location.distance),
                                                                          ]];
            
            
            NSAssert(success, @"%@", [db lastErrorMessage]);
        }];
    }
    
    return success;
}

#pragma mark - Acceleration

- (BOOL)saveAccelerations:(NSArray *)accelerationData sessionId:(int)sessionId {
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath:self.databasePath];
    __block BOOL success = NO;
    for (XEAcceleration *location in accelerationData) {
        [queue inDatabase:^(FMDatabase *db) {
            NSString *query = [[NSString alloc] initWithFormat:@"INSERT INTO Movement (%@) VALUES (?, ?, ?, ?, ?)", @"sessionId, timestamp, x, y, z"];
            success = [db executeUpdate:query withArgumentsInArray:@[
                                                                     @(sessionId),
                                                                     @(location.timestamp),
                                                                     @(location.x),
                                                                     @(location.y),
                                                                     @(location.z)
                                                                     ]];
            
            NSAssert(success, @"%@", [db lastErrorMessage]);
        }];
    }
    
    return success;
}

#pragma mark - Tracking Session

- (void)saveTrackingSession:(XESession *)trackingSession completion:(void (^)(int64_t lastInsertRowId))completion {
    __block BOOL success = NO;
    
    @try {
        [_queue inDatabase:^(FMDatabase *db) {
            NSString *query = [[NSString alloc] initWithFormat:@"INSERT INTO Session (%@) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", @"duration, distance, timestamp, minSpeed, maxSpeed, averageSpeed, minX, minY, minZ, maxX, maxY, maxZ"];
            
            success = [db executeUpdate:query withArgumentsInArray:@[
                                                                     @(trackingSession.duration),
                                                                     @(trackingSession.distance),
                                                                     @(trackingSession.timestamp),
                                                                     @(trackingSession.minSpeed),
                                                                     @(trackingSession.maxSpeed),
                                                                     @(trackingSession.averageSpeed),
                                                                     @(trackingSession.minX),
                                                                     @(trackingSession.minY),
                                                                     @(trackingSession.minZ),
                                                                     @(trackingSession.maxX),
                                                                     @(trackingSession.maxY),
                                                                     @(trackingSession.maxZ),
                                                                     ]];
            
            NSAssert(success, @"%@", [db lastErrorMessage]);
            int64_t lastInsertId = [db lastInsertRowId];
            completion(lastInsertId);
        }];
    }
    @catch (NSException *exception) {
        completion(0);
    }
}



#pragma mark - Statistics

- (NSArray *)getStatistics {
    NSMutableArray __block *dataList = [NSMutableArray new];
    
    [_queue inDatabase:^(FMDatabase *db) {
        NSString *query = [[NSString alloc] initWithFormat:@"select strftime('%W', datetime(timestamp, 'unixepoch'), 'weekday 1') WeekNumber, max(date(datetime(timestamp, 'unixepoch'), 'weekday 1')) WeekStart, max(date(datetime(timestamp, 'unixepoch'), 'weekday 1', '+6 day')) WeekEnd, count(*) as GroupedValues, sum(duration) as TotalDuration,sum(distance) as TotalDistance, min(minSpeed) as MinimumSpeed, max(maxSpeed) as MaximumSpeed, avg(averageSpeed) as AverageSpeed, min(minX) as MinimumX, min(minY) as MinimumY, min(minZ) as MinimumZ, max(maxX) as MaximumX, max(maxY) as MaximumY, max(maxZ) as MaximumZ from Session group by WeekNumber order by WeekStart desc"];
        
        FMResultSet *resultSet = [db executeQuery:query];
        
        while ([resultSet next]) {
            XEStatistics *stat = [XEStatistics new];
            stat.startDate = [resultSet stringForColumn:@"WeekStart"];
            stat.endDate = [resultSet stringForColumn:@"WeekEnd"];
            stat.totalDuration = [resultSet doubleForColumn:@"TotalDuration"];
            stat.totalDistance = [resultSet doubleForColumn:@"TotalDistance"];
            stat.minSpeed = [resultSet doubleForColumn:@"MinimumSpeed"];
            stat.maxSpeed = [resultSet doubleForColumn:@"MaximumSpeed"];
            stat.averageSpeed = [resultSet doubleForColumn:@"AverageSpeed"];
            stat.maxX = [resultSet doubleForColumn:@"MinimumX"];
            stat.maxY = [resultSet doubleForColumn:@"MinimumY"];
            stat.maxZ = [resultSet doubleForColumn:@"MinimumZ"];
            stat.minX = [resultSet doubleForColumn:@"MaximumX"];
            stat.minY = [resultSet doubleForColumn:@"MaximumY"];
            stat.minZ = [resultSet doubleForColumn:@"MaximumZ"];
            
            
            [dataList addObject:stat];
        }
        
        [resultSet close];
    }];
    
    return dataList;
}
@end
