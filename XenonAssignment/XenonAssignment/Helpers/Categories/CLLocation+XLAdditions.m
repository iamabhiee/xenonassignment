//
//  CLLocation+XLAdditions.m
//  XenonAssignment
//
//  Created by Abhishek Sheth on 13/07/15.
//  Copyright (c) 2015 Abhishek. All rights reserved.
//

#import "CLLocation+XLAdditions.h"

@implementation CLLocation (XLAdditions)

- (double)speedInKMPH {
    return [self speed] * 3.6;
}

@end
