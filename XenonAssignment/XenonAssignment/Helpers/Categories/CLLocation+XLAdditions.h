//
//  CLLocation+XLAdditions.h
//  XenonAssignment
//
//  Created by Abhishek Sheth on 13/07/15.
//  Copyright (c) 2015 Abhishek. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

@interface CLLocation (XLAdditions)

- (double)speedInKMPH;

@end
