//
//  XEDatabaseHelper.h
//  XenonAssignment
//
//  Created by Abhishek Sheth on 13/07/15.
//  Copyright (c) 2015 Abhishek. All rights reserved.
//

#import <Foundation/Foundation.h>

@class XESession;

@interface XEDatabaseHelper : NSObject

+ (instancetype)sharedInstance;

- (BOOL)saveLocations:(NSMutableArray *)locations sessionId:(int)sessionId;
- (BOOL)saveAccelerations:(NSArray *)accelerationData sessionId:(int)sessionId;
- (void)saveTrackingSession:(XESession *)trackingSession completion:(void (^)(int64_t lastInsertRowId))completion;

- (NSArray *)getStatistics;

@end
