//
//  XEStatisticsViewCell.h
//  XenonAssignment
//
//  Created by Abhishek Sheth on 13/07/15.
//  Copyright (c) 2015 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>
@class XEStatistics;

@interface XEStatisticsViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTotalDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalTime;
@property (weak, nonatomic) IBOutlet UILabel *lblMinimumSpeed;
@property (weak, nonatomic) IBOutlet UILabel *lblMaximumSpeed;
@property (weak, nonatomic) IBOutlet UILabel *lblAverageSpeed;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxX;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxY;
@property (weak, nonatomic) IBOutlet UILabel *lblMaxZ;
@property (weak, nonatomic) IBOutlet UILabel *lblMinX;
@property (weak, nonatomic) IBOutlet UILabel *lblMinY;
@property (weak, nonatomic) IBOutlet UILabel *lblMinZ;

- (void)configureWithStatistics:(XEStatistics *)statistics;
@end
