//
//  XEStatisticsViewCell.m
//  XenonAssignment
//
//  Created by Abhishek Sheth on 13/07/15.
//  Copyright (c) 2015 Abhishek. All rights reserved.
//

#import "XEStatisticsViewCell.h"
#import "XEStatistics.h"

@implementation XEStatisticsViewCell

- (void)configureWithStatistics:(XEStatistics *)statistics {
    self.lblAverageSpeed.text = [NSString stringWithFormat:@"%.2f Kmph", statistics.averageSpeed];
    self.lblMaximumSpeed.text = [NSString stringWithFormat:@"%.2f Kmph", statistics.maxSpeed];
    self.lblMinimumSpeed.text = [NSString stringWithFormat:@"%.2f Kmph", statistics.minSpeed];
    self.lblTotalDistance.text = [NSString stringWithFormat:@"%.2f Km", statistics.totalDistance / 1000];
    
    double seconds = fmod(statistics.totalDuration, 60.0);
    double minutes = fmod(trunc(statistics.totalDuration / 60.0), 60.0);
    double hours = trunc(statistics.totalDuration / 3600.0);
    self.lblTotalTime.text = [NSString stringWithFormat:@"%02.0f:%02.0f:%02.0f", hours, minutes, seconds];
    
    self.lblMaxX.text = [NSString stringWithFormat:@"%.2f", statistics.maxX];
    self.lblMaxY.text = [NSString stringWithFormat:@"%.2f", statistics.maxY];
    self.lblMaxZ.text = [NSString stringWithFormat:@"%.2f", statistics.maxZ];
    self.lblMinX.text = [NSString stringWithFormat:@"%.2f", statistics.minX];
    self.lblMinY.text = [NSString stringWithFormat:@"%.2f", statistics.minY];
    self.lblMinZ.text = [NSString stringWithFormat:@"%.2f", statistics.minZ];
    
}

@end
